layui.define(['jquery'], function (exports) {
    var $ = layui.$;

    //t:element对象
    var render = function (t, p, r, u) {
        p = p === null || p === undefined || p === true || p === "true";
        r = isNaN(r) ? 500 : r;
        u = isNaN(u) ? 100 : u;
        var s = function (x) {
            var v = "";
            for (var w = 0; w < x.length; w++) {
                if (!isNaN(x.charAt(w))) {
                    return v
                } else {
                    v += x.charAt(w)
                }
            }
        },
            i = function (x) {
                var v = "";
                for (var w = x.length - 1; w >= 0; w--) {
                    if (!isNaN(x.charAt(w))) {
                        return v
                    } else {
                        v = x.charAt(w) + v
                    }
                }
            },
            q = function (w, v) {
                if (!v) {
                    return w
                }
                if (!/^[0-9]+.?[0-9]*$/.test(w)) {
                    return w
                }
                w = w.toString();
                return w.replace(w.indexOf(".") > 0 ? /(\d)(?=(\d{3})+(?:\.))/g : /(\d)(?=(\d{3})+(?:$))/g, "$1,")
            };
        $(t).each(function () {
            var B = $(this);
            var y = B.data("num");
            if (!y) {
                y = B.text().replace(/,/g, "");
                // B.data("num", y);
            }
            var A = "INPUT,TEXTAREA".indexOf(B.get(0).tagName) >= 0;
            var H = s(y.toString()),
                E = i(y.toString());
            var G = y.toString().replace(H, "").replace(E, "");
            if (isNaN(G * 1) || G === "0") {
                A ? B.val(y) : B.html(y);
                // console.log('animateNum Obj', B);
                // return console.error("not a number");
            }
            var v = G.split(".");
            var C = v[1] ? v[1].length : 0;
            var x = 0,
                D = G;
            if (Math.abs(D * 1) > 10) {
                x = parseFloat(v[0].substring(0, v[0].length - 1) + (v[1] ? ".0" + v[1] : ""))
            }
            var z = (D - x) / u,
                F = 0;
            var w = setInterval(function () {
                var I = H + q(x.toFixed(C), p) + E;
                A ? B.val(I) : B.html(I);
                x += z;
                F++;
                if (Math.abs(x) >= Math.abs(D * 1) || F > 5000) {
                    I = H + q(D, p) + E;
                    A ? B.val(I) : B.html(I);
                    clearInterval(w)
                }
            }, r / u)
        })
    }

    exports('animateNum', {
        render: render
    });
});